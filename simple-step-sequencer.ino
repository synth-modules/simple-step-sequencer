const byte latchPin = 8;
const byte clockPin = 12;
const byte dataPin = 11;

byte ledArray_1 [17];
byte ledArray_2 [17];
byte tuneArray [17];

int SPEED;
const byte tuneBP = 6;
const byte tunePot = 1;
byte STEPS = 16;
boolean PLAY = 0;
boolean JETON;
int NOTE = 16;

void setup() {
  // put your setup code here, to run once:
//  Serial.begin(9600);

  pinMode(latchPin, OUTPUT);

  pinMode(2, INPUT); // BP_1 >>>
  pinMode(6, INPUT); // BP_3 >>>
  pinMode(4, OUTPUT); // >>> CV_GATE 
  pinMode(5, OUTPUT); // >>> CV_TUNE
  pinMode(7, INPUT); // BP_2 >>>

  //  for (byte i = 0; i < 8; i++) {
  //    ledArray_1 [i] = pow(2, i);
  //    ledArray_2 [i+8] = pow(2, i);
  //  }

  ledArray_1 [0] = 0xFE;
  ledArray_1 [1] = 0xFD;
  ledArray_1 [2] = 0xFB;
  ledArray_1 [3] = 0xF7;
  ledArray_1 [4] = 0xEF;
  ledArray_1 [5] = 0xDF;
  ledArray_1 [6] = 0xBF;
  ledArray_1 [7] = 0x7F;
  ledArray_1 [8] = 0xFF;
  ledArray_1 [9] = 0xFF;
  ledArray_1 [10] = 0xFF;
  ledArray_1 [11] = 0xFF;
  ledArray_1 [12] = 0xFF;
  ledArray_1 [13] = 0xFF;
  ledArray_1 [14] = 0xFF;
  ledArray_1 [15] = 0xFF;
  ledArray_1 [16] = 0xFF;

  ledArray_2 [0] = 0xFF;
  ledArray_2 [1] = 0xFF;
  ledArray_2 [2] = 0xFF;
  ledArray_2 [3] = 0xFF;
  ledArray_2 [4] = 0xFF;
  ledArray_2 [5] = 0xFF;
  ledArray_2 [6] = 0xFF;
  ledArray_2 [7] = 0xFF;
  ledArray_2 [8] = 0xFE;
  ledArray_2 [9] = 0xFD;
  ledArray_2 [10] = 0xFB;
  ledArray_2 [11] = 0xF7;
  ledArray_2 [12] = 0xEF;
  ledArray_2 [13] = 0xDF;
  ledArray_2 [14] = 0xBF;
  ledArray_2 [15] = 0x7F;
  ledArray_2 [16] = 0xFF;

  tuneArray [0] = 207;
  tuneArray [1] = 0;
  tuneArray [2] = 190;
  tuneArray [3] = 0;
  tuneArray [4] = 0;
  tuneArray [5] = 0;
  tuneArray [6] = 190;
  tuneArray [7] = 207;

  for (byte i = 8; i < STEPS; i++) {
    tuneArray [i] = 255 / 8 * (i - 8);
  }

//  for (byte i = 0; i < STEPS; i++) {
//    tuneArray[i] = 0;
//  }

  SPEED = analogRead(A0);
  END_SETUP ();
}

void loop() {
  // put your main code here, to run repeatedly:
  if (PLAY == 1) {
    SEQUENCE_PLAY();
  }
  else {
    SEQUENCE_STOP();
  }
}

void SEQUENCE_STOP() {
  analogWrite(5, tuneArray[NOTE]);

  digitalWrite(latchPin, 0);
  shiftOut(dataPin, clockPin, ledArray_1 [NOTE]);
  shiftOut(dataPin, clockPin, ledArray_2 [NOTE]);
  digitalWrite(latchPin, 1);

  if (NOTE != STEPS) {
    digitalWrite(4, HIGH);
  }
  else {
    digitalWrite(4, LOW);
  }

  for (byte i = 0; i < 10; i++) {
    delay (2);
    CHECK_STATE();
    EDIT_NOTE();
  }
}

void SEQUENCE_PLAY() {
  for (byte i = 0; i < STEPS; i++) {
    if (PLAY == 1) {
      analogWrite(5, tuneArray[i]);

      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, ledArray_1 [i]);
      shiftOut(dataPin, clockPin, ledArray_2 [i]);
      digitalWrite(latchPin, 1);

      // Gate ON
      digitalWrite(4, HIGH);
      delay(5);

      for (byte i = 0; i < 20; i++) {
        delay(SPEED / 20);
        CHECK_STATE();
        EDIT_NOTE();
      }

      // Gate OFF
      digitalWrite(4, LOW);
    }
  }
}

void CHECK_STATE() {
  // Jeton est à 0 quand les BP sont relâchés
  if (digitalRead(2) == HIGH && digitalRead(7) == HIGH) {
    JETON = 0;
  }

  // Quand Jeton est à zéro
  if (JETON == 0) {
    // Si les deux BP sont appuyés simultanément
    // On change le mode de jeu (PLAY / STOP)
    // Et le Jeton passe à 1
    if (digitalRead(2) == LOW && digitalRead(7) == LOW) {
      delay (20);
      PLAY = !PLAY;
      NOTE = 16;
      JETON = 1;
    }

    // Si le BP_1 seul est appuyé
    // La sélection passe à la note inférieure
    // Et le Jeton passe à 1
    // Si la note est inférieure à la note 0,
    // on sélectionne la note fictive 16
    if (digitalRead(2) == LOW && digitalRead(7) == HIGH) {
      delay(20);
      if (digitalRead(7) != LOW) {
        NOTE--;
        JETON = 1;
        if (NOTE < 0) {
          NOTE = 16;
        }
        if (PLAY == 0) {
          digitalWrite(4, LOW);
        }
      }
    }

    // Si le BP_2 seul est appuyé
    // La sélection passe à la note supérieure
    // Et le Jeton passe à 1
    // Si la note est supérieure à la note fictive 16,
    // on sélectionne la note 0
    if (digitalRead(2) == HIGH && digitalRead(7) == LOW) {
      delay(20);
      if (digitalRead(2) != LOW) {
        NOTE++;
        JETON = 1;
        if (NOTE >= STEPS) {
          NOTE = 0;
        }
        if (PLAY == 0) {
          digitalWrite(4, LOW);
        }
      }
    }
  }

  // On actualise la valeur de SPEED
  SPEED = analogRead(A0);
  STEPS = map(analogRead(A2), 0, 1023, 1, 16); 
}

void EDIT_NOTE() {
  switch (NOTE) {
    case 0:
      if (digitalRead(tuneBP) == LOW) {
        tuneArray [0] = analogRead(tunePot) / 4;
      }
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, ledArray_1 [0]);
      shiftOut(dataPin, clockPin, 0xFF);
      digitalWrite(latchPin, 1);
      break;
    case 1:
      if (digitalRead(tuneBP) == LOW) {
        tuneArray [1] = analogRead(tunePot) / 4;
      }
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, ledArray_1 [1]);
      shiftOut(dataPin, clockPin, 0xFF);
      digitalWrite(latchPin, 1);
      break;
    case 2:
      if (digitalRead(tuneBP) == LOW) {
        tuneArray [2] = analogRead(tunePot) / 4;
      }
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, ledArray_1 [2]);
      shiftOut(dataPin, clockPin, 0xFF);
      digitalWrite(latchPin, 1);
      break;
    case 3:
      if (digitalRead(tuneBP) == LOW) {
        tuneArray [3] = analogRead(tunePot) / 4;
      }
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, ledArray_1 [3]);
      shiftOut(dataPin, clockPin, 0xFF);
      digitalWrite(latchPin, 1);
      break;
    case 4:
      if (digitalRead(tuneBP) == LOW) {
        tuneArray [4] = analogRead(tunePot) / 4;
      }
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, ledArray_1 [4]);
      shiftOut(dataPin, clockPin, 0xFF);
      digitalWrite(latchPin, 1);
      break;
    case 5:
      if (digitalRead(tuneBP) == LOW) {
        tuneArray [5] = analogRead(tunePot) / 4;
      }
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, ledArray_1 [5]);
      shiftOut(dataPin, clockPin, 0xFF);
      digitalWrite(latchPin, 1);
      break;
    case 6:
      if (digitalRead(tuneBP) == LOW) {
        tuneArray [6] = analogRead(tunePot) / 4;
      }
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, ledArray_1 [6]);
      shiftOut(dataPin, clockPin, 0xFF);
      digitalWrite(latchPin, 1);
      break;
    case 7:
      if (digitalRead(tuneBP) == LOW) {
        tuneArray [7] = analogRead(tunePot) / 4;
      }
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, ledArray_1 [7]);
      shiftOut(dataPin, clockPin, 0xFF);
      digitalWrite(latchPin, 1);
      break;
    case 8:
      if (digitalRead(6) == LOW) {
        tuneArray [8] = analogRead(tunePot) / 4;
      }
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, 0xFF);
      shiftOut(dataPin, clockPin, ledArray_2 [8]);
      digitalWrite(latchPin, 1);
      break;
    case 9:
      if (digitalRead(6) == LOW) {
        tuneArray [9] = analogRead(tunePot) / 4;
      }
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, 0xFF);
      shiftOut(dataPin, clockPin, ledArray_2 [9]);
      digitalWrite(latchPin, 1);
      break;
    case 10:
      if (digitalRead(6) == LOW) {
        tuneArray [10] = analogRead(tunePot) / 4;
      }
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, 0xFF);
      shiftOut(dataPin, clockPin, ledArray_2 [10]);
      digitalWrite(latchPin, 1);
      break;
    case 11:
      if (digitalRead(6) == LOW) {
        tuneArray [11] = analogRead(tunePot) / 4;
      }
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, 0xFF);
      shiftOut(dataPin, clockPin, ledArray_2 [11]);
      digitalWrite(latchPin, 1);
      break;
    case 12:
      if (digitalRead(6) == LOW) {
        tuneArray [12] = analogRead(tunePot) / 4;
      }
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, 0xFF);
      shiftOut(dataPin, clockPin, ledArray_2 [12]);
      digitalWrite(latchPin, 1);
      break;
    case 13:
      if (digitalRead(6) == LOW) {
        tuneArray [13] = analogRead(tunePot) / 4;
      }
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, 0xFF);
      shiftOut(dataPin, clockPin, ledArray_2 [13]);
      digitalWrite(latchPin, 1);
      break;
    case 14:
      if (digitalRead(6) == LOW) {
        tuneArray [14] = analogRead(tunePot) / 4;
      }
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, 0xFF);
      shiftOut(dataPin, clockPin, ledArray_2 [14]);
      digitalWrite(latchPin, 1);
      break;
    case 15:
      if (digitalRead(6) == LOW) {
        tuneArray [15] = analogRead(tunePot) / 4;
      }
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, 0xFF);
      shiftOut(dataPin, clockPin, ledArray_2 [15]);
      digitalWrite(latchPin, 1);
      break;
    case 16:
      if (digitalRead(6) == LOW) {
        tuneArray [16] = analogRead(tunePot)*0;
      }
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, 0xFF);
      shiftOut(dataPin, clockPin, 0xFF);
      digitalWrite(latchPin, 1);
      break;
  }
}

void END_SETUP() {
  for (byte i = 0; i < STEPS; i++) {
    analogWrite(5, tuneArray[i]);
    digitalWrite(4, HIGH);

    digitalWrite(latchPin, 0);
    shiftOut(dataPin, clockPin, ledArray_1 [i]);
    shiftOut(dataPin, clockPin, ledArray_2 [i]);
    digitalWrite(latchPin, 1);

    delay(100);
    digitalWrite(4, LOW);
    delay(50);
  }

  for (byte i = 0; i < 16; i++) {
    tuneArray [i] = 0;
  }
}

void shiftOut(int myDataPin, int myClockPin, byte myDataOut) {
  // This shifts 8 bits out MSB first,
  //on the rising edge of the clock,
  //clock idles low

  //internal function setup
  char i = 0;
  boolean pinState;
  pinMode(myClockPin, OUTPUT);
  pinMode(myDataPin, OUTPUT);

  //clear everything out just in case to
  //prepare shift register for bit shifting
  digitalWrite(myDataPin, 0);
  digitalWrite(myClockPin, 0);

  //for each bit in the byte myDataOut�
  //NOTICE THAT WE ARE COUNTING DOWN in our for loop
  //This means that %00000001 or "1" will go through such
  //that it will be pin Q0 that lights.
  for (i = 7; i >= 0; i--)  {
    digitalWrite(myClockPin, 0);

    //if the value passed to myDataOut and a bitmask result
    // true then... so if we are at i=6 and our value is
    // %11010100 it would the code compares it to %01000000
    // and proceeds to set pinState to 1.
    if ( myDataOut & (1 << i) ) {
      pinState = 1;
    }
    else {
      pinState = 0;
    }

    //Sets the pin to HIGH or LOW depending on pinState
    digitalWrite(myDataPin, pinState);
    //register shifts bits on upstroke of clock pin
    digitalWrite(myClockPin, 1);
    //zero the data pin after shift to prevent bleed through
    digitalWrite(myDataPin, 0);
  }

  //stop shifting
  digitalWrite(myClockPin, 0);
}
